'use strict'
var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

prodEnv.BASE_HUB_URL = '"http://localhost:59471/"'

prodEnv.NODE_ENV = '"development"'

module.exports = prodEnv
